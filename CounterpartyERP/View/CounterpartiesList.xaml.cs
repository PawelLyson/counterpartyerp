﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CounterpartyERP.View
{
    /// <summary>
    /// Logika interakcji dla klasy CounterpartiesList.xaml
    /// </summary>
    public partial class CounterpartiesList : Window
    {
        public CounterpartiesList()
        {
            InitializeComponent();
        }
    }
    public class OneMoreConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType != typeof(string))
            {
                throw new InvalidOperationException("Target should be string");
            }
            else
            {
                return ((int)value) + 1;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
