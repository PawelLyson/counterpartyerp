﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterpartyERP.Model
{
    [Table("Kontrahenci")]
    public class Counterparty
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [StringLength(11)]
        public string NIP { get; set; }
        [StringLength(14)]
        public string REGON { get; set; }
        [StringLength(11)]
        public string PESEL { get; set; }
        [Required]
        [StringLength(25)]
        [Column("Nazwa_krotka")]
        public string ShortName { get; set; }
        [Required]
        [Column("Nazwa_pelna")]
        public string FullName { get; set; }
        [Column("Opis")]
        public string Description { get; set; }
        [Column("Strona_WWW")]
        public string WebsiteUrl { get; set; }
        public List<Contact> Contacts { get; set; }

        [Required]
        [Column("Adres_fakturowy")]
        public int? InvoiceAddressId { get; set; }
        public virtual Address InvoiceAddress { get; set; }
        [Column("Adres_korespondencyjny")]
        public int? CorrespondenceAddressId { get; set; }
        public virtual Address CorrespondenceAddress { get; set; }
    }
    [Table("Kontakt")]
    public class Contact
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        [Column("Id_kontrahenta")]
        public int CounterpartyId { get; set; } //klucz obcy
        public virtual Counterparty Counterparty { get; set; }
        [Required]
        [StringLength(2)]
        [Column("Piorytet")]
        public int Priority { get; set; }
        [StringLength(15)]
        [Column("Numer_telefonu")]
        public string TelephoneNumber { get; set; }
        [Column("Email")]
        public string EmailAddress { get; set; }
        [Column("Opis")]
        public string Description { get; set; }
    }

    [Table("Adres")]
    public class Address
    {
        [Key]
        [Required]
        [Column("Id_adres")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [Column("Kraj")]
        public string Country { get; set; }
        [Required]
        [Column("Miasto")]
        public string City { get; set; }
        [Required]
        [Column("Ulica")]
        public string Street { get; set; }
        [Required]
        [Column("Numer_budynku")]
        public string BuildingNumber { get; set; }
        [Column("Numer_lokalu")]
        public string FlatNumber { get; set; }
        [Required]
        [Column("Kod_pocztowy")]
        public string ZipCode { get; set; }
    }
}
