﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CounterpartyERP.Model
{
    public class CounterpartiesDatabaseConnection : DbContext
    {
        public DbSet<Counterparty> Counterparties { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public IQueryable<Model.CounterpartiesWithAdressView> CounterpartiesWithAdressesView //widok z bazy danych
        {
            get
            {
                return (from kl in Counterparties
                        join adr in Addresses
                        on kl.InvoiceAddressId equals adr.Id
                        select new CounterpartiesWithAdressView
                        {
                            Id = kl.Id,
                            NIP = kl.NIP,
                            PESEL = kl.PESEL,
                            ShortName = kl.ShortName,
                            Street = adr.Street,
                            City = adr.City,
                            Country = adr.Country,
                            ZipCode = adr.ZipCode
                        });
            }
        }
        public IQueryable<CounterpartiesWithAdressView> GetCounterpartyWithAdressesView()
        {
            IQueryable<CounterpartiesWithAdressView> view = (from kl in Counterparties
                              join adr in Addresses
                              on kl.InvoiceAddressId equals adr.Id
                              select new CounterpartiesWithAdressView
                              {
                                  Id = kl.Id,
                                  NIP = kl.NIP,
                                  PESEL = kl.PESEL,
                                  ShortName = kl.ShortName,
                                  Street = adr.Street,
                                  City = adr.City,
                                  Country = adr.Country,
                                  ZipCode = adr.ZipCode
                              });
            return view;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string path = System.IO.Path.Combine(System.Environment.CurrentDirectory, @"Database/Clients.db3");
            optionsBuilder.UseSqlite($"FileName={path}");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>() // klucz obcy
                .HasOne(p => p.Counterparty)
                .WithMany(b => b.Contacts)
                .HasForeignKey(p => p.CounterpartyId)
                .HasConstraintName("FK_Contact_Counterparty");
        }
    }

    public class CounterpartiesGetDatabaseOperatons
    {
        protected CounterpartiesDatabaseConnection Connection;
        public CounterpartiesGetDatabaseOperatons()
        {
            Connection = new CounterpartiesDatabaseConnection();
        }

        public Counterparty GetCounterparty(int counterpartyId)
        {
            return Connection.Counterparties.First(c => c.Id == counterpartyId);
        }
        public IQueryable<Contact> GetContactsByCounterparty(int counterpartyId)
        {
            return Connection.Contacts.Where(a => a.CounterpartyId == counterpartyId);
        }
        public Address GetAddress(int addressId)
        {
            return Connection.Addresses.Single(c => c.Id == addressId);
        }

        public IQueryable<CounterpartiesWithAdressView> GetCounterpartyWithAdressesView()
        {
            return Connection.GetCounterpartyWithAdressesView();
        }
    }

    public class CounterpartiesRemoveDatabaseOperations : CounterpartiesGetDatabaseOperatons
    {
        public void RemoveAddresses(int addressId)
        {
            Connection.Addresses.Remove(GetAddress(addressId));
        }
        public void RemoveCounterparty(Counterparty counterparty)
        {
            RemoveContactsFromCounterparty(counterparty.Id);
            RemoveAddresses(counterparty.InvoiceAddressId.GetValueOrDefault());
            try
            {
                RemoveAddresses(counterparty.CorrespondenceAddressId.Value);
            }
            catch (System.InvalidOperationException e)
            {
                //TODO: Dodanie informacji debug
                //DEBUG 
            }
            Connection.Counterparties.Remove(counterparty);
        }
        public void RemoveCounterparty(int counterpartyId)
        {
            RemoveCounterparty(GetCounterparty(counterpartyId));
        }
        public void RemoveContact(IQueryable<Contact> contacts)
        {
            Connection.Contacts.RemoveRange(contacts);
        }
        public void RemoveContact(Contact contact)
        {
            Connection.Contacts.Remove(contact);
        }
        public void RemoveContact(int contactId)
        {
            RemoveContact(Connection.Contacts.Find(contactId));
        }
        public void RemoveContactsFromCounterparty(int counterpartyId)
        {
            Connection.Contacts.RemoveRange(Connection.Contacts.Where(a => a.CounterpartyId == counterpartyId));
        }
    }
    public class CounterpartiesAddDatabaseOperations : CounterpartiesRemoveDatabaseOperations
    {
        public void AddContact(Contact contact)
        {
            Connection.Contacts.Add(contact);
        }
        public void AddAddress(Address address)
        {
            Connection.Addresses.Add(address);
        }
        public void AddCounterparty(Counterparty counterparty)
        {
            Connection.Counterparties.Add(counterparty);
        }
    }
    public class CounterpartiesDatabaseOperations : CounterpartiesAddDatabaseOperations
    {
        public void DiscardChanges()
        {
            Connection.ChangeTracker.Entries()
                .Where(e => e.Entity != null).ToList()
                .ForEach(e => e.State = EntityState.Detached);
        }
        public void SaveChanges()
        {
            Connection.SaveChanges();
        }
    }

}
