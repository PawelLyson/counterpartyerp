﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterpartyERP.Model
{
    public class CounterpartiesOperations
    {
        private Model.CounterpartiesDatabaseOperations DatabaseConnection;
        public CounterpartiesOperations()
        {
            DatabaseConnection = new CounterpartiesDatabaseOperations();
        }
        public IQueryable<CounterpartiesWithAdressView> GetCounterpartiesElements()
        {
            return DatabaseConnection.GetCounterpartyWithAdressesView();
        }
    }
}
