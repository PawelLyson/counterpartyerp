﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterpartyERP.Model
{
    public class CounterpartiesWithAdressView
    {
        public int Id { get; set; }
        public string NIP { get; set; }
        public string PESEL { get; set; }
        public string ShortName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string NIP_PESEL { get { return NIP ?? PESEL; } }
    }
}
