﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace CounterpartyERP.ViewModel
{
    class CounterpartiesList : DefaultViewModel
    {
        private View.CounterpartiesList WindowCounterpartiesList;
        private Model.CounterpartiesOperations CounterpartiesOperations;
        private Dispatcher Dispatcher = Dispatcher.CurrentDispatcher;

        public CounterpartiesList()
        {
            WindowCounterpartiesList = new View.CounterpartiesList()
            {
                DataContext = this
            };
            CounterpartiesOperations = new Model.CounterpartiesOperations();

        }
        public void ShowWindow()
        {
            WindowCounterpartiesList.Show();
            Task.Run(() => LoadCounterpartiesElementsAsync()).ConfigureAwait(false);

        }
        private void LoadCounterpartiesElementsAsync()
        {
            IQueryable<Model.CounterpartiesWithAdressView> counterpartiesViewList = CounterpartiesOperations.GetCounterpartiesElements();
            foreach (var element in counterpartiesViewList)
            {
                Dispatcher.Invoke(() => CounterpartyWithAdressViewList.Add(element));
            }

        }
        private ObservableCollection<Model.CounterpartiesWithAdressView> CounterpartyWithAdressViewListValue = new ObservableCollection<Model.CounterpartiesWithAdressView>();
        public ObservableCollection<Model.CounterpartiesWithAdressView> CounterpartyWithAdressViewList
        {
            get
            {
                return CounterpartyWithAdressViewListValue;
            }
            set
            {
                CounterpartyWithAdressViewListValue = value;
                NotifyPropertyChanged();
            }
        }
    }
}
