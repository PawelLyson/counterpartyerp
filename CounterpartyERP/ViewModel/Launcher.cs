﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CounterpartyERP.ViewModel
{
    class Launcher
    {
        private View.Launcher WindowLauncher;
        private ViewModel.CounterpartiesList ViewModelCounterpartiesList;
        public ICommand CounterpartyButtonCommand { get; private set; }
        public Launcher()
        {
            WindowLauncher = new View.Launcher()
            {
                DataContext = this
            };
            ViewModelCounterpartiesList = new CounterpartiesList();
            CounterpartyButtonCommand = new RelayCommand(CounterpartyButtonAction);
            WindowLauncher.Show();
        }

        public void CounterpartyButtonAction(object sender)
        {
            ViewModelCounterpartiesList.ShowWindow();
        }
    }
}
